var collapseBtn = document.querySelector('.collapse-btn');
var sidebar = document.querySelector('.dashboard-content__sidebar');
var mainSection = document.querySelector('.dashboard__application-section');
collapseBtn.addEventListener('click', function() {
  sidebar.classList.toggle('collapsed');
  mainSection.classList.toggle('collapsed');
});
